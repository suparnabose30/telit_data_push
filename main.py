import google.cloud import bigquery
import logging
import io
import datetime

# Construct a BigQuery client object.
bq_client = bigquery.Client()

logger.basicConfig(filename="telit_data_push_log.log" , filemode='a' , level=logging.INFO , format='%(asctime)s - %(levelname)s: %(message)s',\
    datefmt='%m%d%Y %I:%M:%S %p')

def telit_data_push(request):
    try:
        logger.info("Successfully run the cloud function")
    except Exception as ex:
        logger.error("Error in TelIt Data Push")